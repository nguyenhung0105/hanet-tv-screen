const express = require("express");
const bodyParser = require("body-parser");
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
const cors = require("cors")({origin:true});
app.use(cors);

// CREATE SERVER SOCKET IO
const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server,{
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    } });

io.on("connection", (socket) => {
	console.log("a user connected");
	socket.on("disconnect", () => {
		console.log("user disconnected");
	});
});


app.post("/camera-event", (req, res) => {
	console.log("data", req.body);
	const data = req.body;
	io.of("/").emit("checkIn", req.body);
	io.emit(
		"chat message",
		data.deviceID + ":" + data.placeName + " " + data.personName
	);
	res.status(200).json({ status: "sent" });
});

var s = server.listen(3001, function () {
	var host = s.address().address;
	var port = s.address().port;

	console.log("Example app listening at http://%s:%s", host, port);
});

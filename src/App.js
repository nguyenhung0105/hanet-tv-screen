import "./App.scss";
import Container from "./Container";
import Header from "./Header";
import React from "react";
// import App1 from "./animation";

function App() {
	return (
		<div className='app'>
			<Header />
			<Container />
			{/* <App1 /> */}
		</div>
	);
}

export default App;

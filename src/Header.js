import React from "react";
import logo1 from "./logo1.png";
import "./Header.scss";

const Header = () => {
	return (
		<div className='header'>
			<div className='header__wrapper-logo'>
				<img src={logo1} alt='logo' />
			</div>
		</div>
	);
};

export default Header;

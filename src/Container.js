import React, { useEffect, useState } from "react";
import io from "socket.io-client";
import "./Container.scss";
import { useTransition, animated } from "react-spring";

import moment from "moment";

let data = [
	// {
	// 	personName: "Hung",
	// 	detected_image_url:
	// 		"https://static.hanet.ai/face/upload/C21283M384/2021/11/22/a255afec-5af1-4a4f-b22b-46ef5e8be374.jpg",
	// },
	// {
	// 	personName: "Rare Windsadc",
	// 	detected_image_url:
	// 		"https://static.hanet.ai/face/upload/C21283M384/2021/11/22/a255afec-5af1-4a4f-b22b-46ef5e8be374.jpg",
	// },
	// {
	// 	personName: "Rare Windasdc",
	// 	detected_image_url:
	// 		"https://static.hanet.ai/face/upload/C21283M384/2021/11/22/a255afec-5af1-4a4f-b22b-46ef5e8be374.jpg",
	// },
];

function dateFormatter(cell) {
	if (!cell) {
		return "";
	}
	return `${moment(cell).format("HH:mm:ss ")}`;
}

const Container = () => {
	const [user, setUser] = useState("");

	const [list, setList] = useState(data);

	const item_quantity = process.env.REACT_APP_ITEM_QUANTITY;

	const height = 50;
	const width = 410;
	const transitions = useTransition(
		list.map((data, i) => ({ ...data, x: i * width, y: i * height })),
		(d) => d.id,
		{
			from: { position: "absolute", opacity: 0 },
			leave: { opacity: 0 },
			enter: ({ x, y }) => ({ x, y, opacity: 1 }),
			update: ({ x, y }) => ({ x, y }),
		}
	);

	useEffect(() => {
		const socket = io("http://localhost:3001");
		socket.on("checkIn", (data) => {
			if (data.action_type === "update") {
				console.log("req", data);
				setUser(data);
			}
		});
	}, []);

	// ADD LIST
	useEffect(() => {
		console.log("list", list);

		// if (list.length >= item_quantity) {
		// 	list.shift();
		// }
		// if (user.action_type === "update") {
		// 	setList((prev) => [...prev, user]);
		// }
		if (user.action_type === "update") {
			if (list.length >= item_quantity) {
				setList([...list.slice(1, list.length), user]);
			} else {
				setList([...list, user]);
			}
		}
	}, [user]);

	return (
		<div className='container'>
			<div className='container__slide'>
				{/* <button
					onClick={() =>
						setList([
							...list.slice(1, list.length),
							{
								personName: `list item ${Math.floor(
									Math.random() * 1000
								)}`,
							},
						])
					}
				>
					add first
				</button> */}
				<div className='wrapper-content'>
					{transitions.map(
						({ item, props: { x, y, ...rest }, key }, index) => (
							<animated.div
								key={key}
								style={{
									textAlign: "center",
									transform: x.interpolate(
										(x) => `translate3d(${x}px,0,0)`
									),
									...rest,
								}}
							>
								<>
									{item.personType === "0" ? (
										<div className='wrapper employee'>
											<div className='wrapper__img'>
												<img
													src={
														item.detected_image_url
													}
													alt={`avatar ${item.personName}`}
												/>
											</div>

											<div className='wrapper__description'>
												<p className='time'>
													{dateFormatter(item.time)}
												</p>
												<p>{item.personName}</p>
											</div>
										</div>
									) : (
										<>
											{item.personType === "1" ? (
												<div className='wrapper customer'>
													<div className='wrapper__img'>
														<img
															src={
																item.detected_image_url
															}
															alt={`avatar ${item.personName}`}
														/>
													</div>

													<div className='wrapper__description'>
														<p className='time'>
															{dateFormatter(
																item.time
															)}
														</p>
														<p>
															{user.personName ||
																"Khách"}
														</p>
													</div>
												</div>
											) : (
												<div className='wrapper stranger'>
													<div className='wrapper__img'>
														<img
															src={
																item.detected_image_url
															}
															alt='img'
														/>
													</div>

													<div className='wrapper__description'>
														<p className='time'>
															{dateFormatter(
																item.time
															)}
														</p>
														<p>Khách</p>
													</div>
												</div>
											)}
										</>
									)}
								</>
							</animated.div>
						)
					)}

					{/* ----------------------------------------------------------- */}
					{/* {list.map((item) => {
						return (
							<>
								{item.personType === "0" ? (
									<div className='wrapper employee'>
										<div className='wrapper__img'>
											<img
												src={item.detected_image_url}
												alt={`avatar ${item.personName}`}
											/>
										</div>

										<div className='wrapper__description'>
											<p className='time'>
												{dateFormatter(item.time)}
											</p>
											<p>{item.personName}</p>
										</div>
									</div>
								) : (
									<>
										{item.personType === "1" ? (
											<div className='wrapper customer'>
												<div className='wrapper__img'>
													<img
														src={
															item.detected_image_url
														}
														alt={`avatar ${item.personName}`}
													/>
												</div>

												<div className='wrapper__description'>
													<p className='time'>
														{dateFormatter(
															item.time
														)}
													</p>
													<p>
														{user.personName ||
															"Khách"}
													</p>
												</div>
											</div>
										) : (
											<div className='wrapper stranger'>
												<div className='wrapper__img'>
													<img
														src={
															item.detected_image_url
														}
														alt='img'
													/>
												</div>

												<div className='wrapper__description'>
													<p className='time'>
														{dateFormatter(
															item.time
														)}
													</p>
													<p>Người lạ</p>
												</div>
											</div>
										)}
									</>
								)}
							</>
						);
					})} */}

					{/* EXAMPLE ----------------------------------------*/}
					{/* <div className='wrapper employee'>
						<div className='wrapper__img'>
							<img
								src={
									"https://static.hanet.ai/face/upload/C21283M384/2021/11/22/a255afec-5af1-4a4f-b22b-46ef5e8be374.jpg"
								}
								alt='adfsdf'
							/>
						</div>

						<div className='wrapper__description'>
							<p className='time'>12:02:02</p>
							<p>Phùng hoàng thanh trúc</p>
						</div>
					</div>

					<div className='wrapper stranger'>
						<div className='wrapper__img'>
							<img
								src={
									"https://static.hanet.ai/face/upload/C21283M384/2021/11/22/a255afec-5af1-4a4f-b22b-46ef5e8be374.jpg"
								}
								alt='adfsdf'
							/>
						</div>

						<div className='wrapper__description'>
							<p className='time'>12:02:02</p>
							<p>Hàng Nguyên Hưng</p>
						</div>
					</div>
					<div className='wrapper customer'>
						<div className='wrapper__img'>
							<img
								src={
									"https://static.hanet.ai/face/upload/C21283M384/2021/11/22/a255afec-5af1-4a4f-b22b-46ef5e8be374.jpg"
								}
								alt='adfsdf'
							/>
						</div>

						<div className='wrapper__description'>
							<p className='time'>12:02:02</p>
							<p>Khách</p>
						</div>
					</div> */}
				</div>
			</div>
		</div>
	);
};

export default Container;

// -----------------------------------------------

// console.log(list);
// const wrappers = document.querySelectorAll(".wrapper");
// const imgs = document.querySelectorAll("img");
// if (wrappers.length > 2) {
// 	wrappers[0].classList.add("fade-out");
// 	wrappers.forEach(function (wrapper) {
// 		wrapper.classList.add("move-left");
// 	});
// }
// if (wrappers.length > 1) {
// 	wrappers[0].onanimationend = function () {
// 		wrappers[0].remove();
// 	};
// }
// console.log(wrappers);
// if (wrappers.length > 0) {
// 	wrappers.forEach((wrapper) => {
// 		wrapper.classList.remove("fade-in");
// 	});
// }
// if (imgs.length > 0) {
// 	imgs.forEach((child, i) => child.classList.remove("img-current"));
// }
// if (list > 2 && user.action_type === "update") {
// 	setTimeout(() => {
// 		setList((prev) => [...prev, user]);
// 		console.log(212);
// 	}, 2000);
// } else if (user.action_type === "update") {
// 	setList((prev) => [...prev, user]);
// }
// console.log(list);

# build stage
# FROM node:13-alpine as build-stage

FROM hypriot/rpi-node 
WORKDIR /app
COPY . .
RUN npm install -g create-react-app
RUN npm install  
RUN npm run build


EXPOSE 3000 3001

# production stage
# FROM nginx:1.17-alpine as production-stage
# COPY --from=build-stage /app/dist /usr/share/nginx/html
CMD ["npm", "run", "all"]
